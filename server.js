
let express = require('express');
let bodyParser = require('body-parser');
var swig  = require('swig');  
let mongoose = require('mongoose');
let app = express();
let apiRoutes = require("./routes/api-routes")
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
mongoose.connect('mongodb://localhost/contactInfo');
var db = mongoose.connection;
var port = process.env.PORT || 8080;
app.get('/', function(req, res){  
var template = swig.compileFile(__dirname + '/public/cv.html');  
var output = template({});  
	res.send(output); 
   	});  
    	app.use(express.static('public')); 
app.use('/api', apiRoutes)
app.listen(port, function () {
    console.log("Running contactInfo on port " + port);
});