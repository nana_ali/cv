function validateForm() {
    var yourName = document.myform.yourname.value;
    var message = document.myform.message.value;
    var email = document.myform.email.value;
    var phone = document.myform.phone.value;
    var websiteAddress = document.myform.websiteUrl.value;
    var validfirstUsername = document.myform.yourname.value.match(nameRegex);
    var nameRegex = /^[a-zA-Z\-]+$/;
    var phoneno = /^\(?([0]{2})\)?[. ]?([1-9]{1})[. ]?([0-9]{9,14})$/;
    var emailPattern = /^\(?[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    var websitePattern = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    var count = 0;

    var word = message.split(" ");
    for (i = 0; i < word.length; i++) {
        if (word[i] != "")
            count += 1;
    }


    if (yourName == null || yourName == "") { /*  Name Validation */
        document.getElementById("yourname").style.borderColor = "red";
        document.getElementById("nameLabel").innerHTML = '* Required field';
        document.getElementById("nameLabel").style.color = "red";
        return false;
    } else if (validfirstUsername == null) {
        document.getElementById("yourname").style.borderColor = "red";
        document.getElementById("nameLabel").innerHTML = ' Please enter valid name';
        document.getElementById("nameLabel").style.color = "red";
        return false;
    } else {
        document.getElementById("yourname").style.borderColor = "rgb(56, 224, 14)";
        document.getElementById("nameLabel").innerHTML = 'Your name';
        document.getElementById("nameLabel").style.color = "white";

    }
    if (phone == null || phone == "") { /*  Phone Validation */
        document.getElementById("phone").style.borderColor = "red";
        document.getElementById("phoneLabel").innerHTML = '* Required field';
        document.getElementById("phoneLabel").style.color = "red ";
        return false;
    } else if (document.myform.phone.value.match(phoneno) == null) {
        document.getElementById("phone").style.borderColor = "red";
        document.getElementById("phoneLabel").innerHTML = 'Please enter valid phone no';
        document.getElementById("phoneLabel").style.color = "red";
        return false;
    } else {
        document.getElementById("phone").style.borderColor = "rgb(56, 224, 14)";
        document.getElementById("phoneLabel").innerHTML = 'Your phone';
        document.getElementById("phoneLabel").style.color = "white";
    }
    if (email == null || email == "") { /*  Email Validation */
        document.getElementById("emailField").style.borderColor = "red";
        document.getElementById("emailLabel").innerHTML = '* Required field';
        document.getElementById("emailLabel").style.color = "red";
        return false;
    } else if (document.myform.email.value.match(emailPattern) == null) { /*  Email Validation */
        document.getElementById("emailField").style.borderColor = "red";
        document.getElementById("emailLabel").innerHTML = '* Please enter valid email address';
        document.getElementById("emailLabel").style.color = "red";
        return false;
    } else {
        document.getElementById("emailField").style.borderColor = "rgb(56, 224, 14)";
        document.getElementById("emailLabel").innerHTML = 'Your email';
        document.getElementById("emailLabel").style.color = "white";

    }
    if (websiteAddress == null || websiteAddress == "") { /*  Website Validation */
        document.getElementById("websiteLabel").style.borderColor = "red";
        document.getElementById("websiteLabel").innerHTML = '* Required field';
        document.getElementById("websiteLabel").style.color = "red";
        return false;
    } else if (document.myform.websiteUrl.value.match(websitePattern) == null) { /*  Email Validation */
        document.getElementById("websiteLabel").style.borderColor = "red";
        document.getElementById("websiteLabel").innerHTML = '* Please enter valid Website address';
        document.getElementById("websiteLabel").style.color = "red";
        return false;
    } else {
        document.getElementById("websiteLabel").style.borderColor = "rgb(56, 224, 14)";
        document.getElementById("websiteLabel").innerHTML = 'Your email';
        document.getElementById("websiteLabel").style.color = "white";
    }
    if (message == null || message == "") { /*  Message Validation */
        document.getElementById("message").style.borderColor = "red";
        document.getElementById("messageLabel").innerHTML = '* Required field';
        document.getElementById("messageLabel").style.color = "red";
        return false;
    } else if (count > 30) {
        document.getElementById("message").style.borderColor = "red";
        document.getElementById("messageLabel").innerHTML = '* Your message very long';
        document.getElementById("messageLabel").style.color = "red";
        return false;
    } else {
        document.getElementById("message").style.borderColor = "rgb(56, 224, 14)";
        document.getElementById("messageLabel").innerHTML = 'Your message';
        document.getElementById("messageLabel").style.color = "white";
        return true;
    }
}