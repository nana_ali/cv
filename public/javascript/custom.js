var modal = document.getElementById('myModal');
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function () {
	modal.style.display = "block";
}
span.onclick = function () {
	modal.style.display = "none";
}
window.onclick = function (event) {
	if (event.target == modal) {
		modal.style.display = "none";
	}
}

function myFunction() {
	document.getElementById("namediv").style.backgroundColor = "#44C3E3";
	document.getElementById("edu1").style.color = "#44C3E3";
	document.getElementById("edu2").style.color = "#44C3E3";
	document.getElementById("s1").style.borderLeftColor = " #44C3E3";
	document.getElementById("s2").style.borderLeftColor = " #44C3E3";
	document.getElementById("s3").style.borderLeftColor = " #44C3E3";
	document.getElementById("s4").style.borderLeftColor = " #44C3E3";
	document.getElementById("s5").style.borderLeftColor = " #44C3E3";
	document.getElementById("s6").style.borderLeftColor = " #44C3E3";
	document.getElementById("s7").style.borderLeftColor = " #44C3E3";
	document.getElementById("s8").style.borderLeftColor = " #44C3E3";
	document.getElementById("edu3").style.color = "#44C3E3";
	document.getElementById("left").style.backgroundColor = "#053750";
	document.getElementById("t1").style.color = "#053750";
	document.getElementById("t2").style.color = "#053750";
	document.getElementById("t3").style.color = "#053750";
	document.getElementById("sub1").style.color = "#053750";
	document.getElementById("sub2").style.color = "#053750";
	document.getElementById("sub3").style.color = "#053750";
	document.getElementById("sub4").style.color = "#053750";
	document.getElementById("sub5").style.color = "#053750";
	document.getElementById("sub6").style.color = "#053750";
	document.getElementById("sub7").style.color = "#053750";
	document.getElementById("sub8").style.color = "#053750";
	document.getElementById("sub9").style.color = "#053750";
	document.getElementById("container").style.color = "#FFFFFF";
	document.getElementById("s6").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("s7").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("s8").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("header").style.backgroundColor = "#44C3E3";
	document.getElementById("send").style.backgroundColor = "#44C3E3";
	document.getElementById("yourname").addEventListener("focus", changeNameColor);
	document.getElementById("phone").addEventListener("focus", changePhoneColor);
	document.getElementById("emailField").addEventListener("focus", changeEmailColor);
	document.getElementById("website").addEventListener("focus", changeWebsiteColor);
	document.getElementById("message").addEventListener("focus", changeMessageColor);
	document.getElementById("yourname").addEventListener("blur", changeNameBlurColor);
	document.getElementById("phone").addEventListener("blur", changeBlurPhoneColor);
	document.getElementById("emailField").addEventListener("blur", changeBlurEmailColor);
	document.getElementById("website").addEventListener("blur", changeBlurWebsiteColor);
	document.getElementById("message").addEventListener("blur", changeBlurMessageColor);
	document.getElementById("linkedin").addEventListener("mouseover", changelinkedinColor);
	document.getElementById("emailLink").addEventListener("mouseover", changemailColor);
	document.getElementById("myBtn").addEventListener("mouseover", changeBtnColor);
	document.getElementById("linkedin").addEventListener("mouseout", changelinkedinMouseout);
	document.getElementById("emailLink").addEventListener("mouseout", changeEmailMouseout);
	document.getElementById("myBtn").addEventListener("mouseout", changeBtnMouseout);

}

function myFunction2() {
	document.getElementById("namediv").style.backgroundColor = "#F8B7BB";
	document.getElementById("edu1").style.color = "#F8B7BB";
	document.getElementById("edu2").style.color = "#F8B7BB";
	document.getElementById("s1").style.borderLeftColor = " #F8B7BB";
	document.getElementById("s2").style.borderLeftColor = " #F8B7BB";
	document.getElementById("s3").style.borderLeftColor = " #F8B7BB";
	document.getElementById("s4").style.borderLeftColor = " #F8B7BB";
	document.getElementById("s5").style.borderLeftColor = " #F8B7BB";
	document.getElementById("s6").style.borderLeftColor = " #F8B7BB";
	document.getElementById("s7").style.borderLeftColor = " #F8B7BB";
	document.getElementById("s8").style.borderLeftColor = " #F8B7BB";
	document.getElementById("edu3").style.color = "#F8B7BB";
	document.getElementById("left").style.backgroundColor = "#053750";
	document.getElementById("t1").style.color = "#053750";
	document.getElementById("t2").style.color = "#053750";
	document.getElementById("t3").style.color = "#053750";
	document.getElementById("sub1").style.color = "#053750";
	document.getElementById("sub2").style.color = "#053750";
	document.getElementById("sub3").style.color = "#053750";
	document.getElementById("sub4").style.color = "#053750";
	document.getElementById("sub5").style.color = "#053750";
	document.getElementById("sub6").style.color = "#053750";
	document.getElementById("sub7").style.color = "#053750";
	document.getElementById("sub8").style.color = "#053750";
	document.getElementById("sub9").style.color = "#053750";
	document.getElementById("container").style.color = "#FFFFFF";
	document.getElementById("s6").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("s7").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("s8").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("header").style.backgroundColor = "#F8B7BB";
	document.getElementById("send").style.backgroundColor = "#F8B7BB";
	document.getElementById("yourname").addEventListener("focus", changeNameColor);
	document.getElementById("phone").addEventListener("focus", changePhoneColor);
	document.getElementById("emailField").addEventListener("focus", changeEmailColor);
	document.getElementById("website").addEventListener("focus", changeWebsiteColor);
	document.getElementById("message").addEventListener("focus", changeMessageColor);
	document.getElementById("yourname").addEventListener("blur", changeNameBlurColor);
	document.getElementById("phone").addEventListener("blur", changeBlurPhoneColor);
	document.getElementById("emailField").addEventListener("blur", changeBlurEmailColor);
	document.getElementById("website").addEventListener("blur", changeBlurWebsiteColor);
	document.getElementById("message").addEventListener("blur", changeBlurMessageColor);
	document.getElementById("linkedin").addEventListener("mouseover", changelinkedinColor);
	document.getElementById("emailLink").addEventListener("mouseover", changemailColor);
	document.getElementById("myBtn").addEventListener("mouseover", changeBtnColor);
	document.getElementById("linkedin").addEventListener("mouseout", changelinkedinMouseout);
	document.getElementById("emailLink").addEventListener("mouseout", changeEmailMouseout);
	document.getElementById("myBtn").addEventListener("mouseout", changeBtnMouseout);


}

function myFunction3() {
	document.getElementById("namediv").style.backgroundColor = "#9CC61B";
	document.getElementById("left").style.backgroundColor = "#053750";
	document.getElementById("edu1").style.color = "#9CC61B";
	document.getElementById("edu2").style.color = "#9CC61B";
	document.getElementById("s1").style.borderLeftColor = " #9CC61B";
	document.getElementById("s2").style.borderLeftColor = " #9CC61B";
	document.getElementById("s3").style.borderLeftColor = " #9CC61B";
	document.getElementById("s4").style.borderLeftColor = " #9CC61B";
	document.getElementById("s5").style.borderLeftColor = " #9CC61B";
	document.getElementById("s6").style.borderLeftColor = " #9CC61B";
	document.getElementById("s7").style.borderLeftColor = " #9CC61B";
	document.getElementById("s8").style.borderLeftColor = " #9CC61B";
	document.getElementById("edu3").style.color = "#9CC61B";
	document.getElementById("t1").style.color = "#053750";
	document.getElementById("t2").style.color = "#053750";
	document.getElementById("t3").style.color = "#053750";
	document.getElementById("sub1").style.color = "#053750";
	document.getElementById("sub2").style.color = "#053750";
	document.getElementById("sub3").style.color = "#053750";
	document.getElementById("sub4").style.color = "#053750";
	document.getElementById("sub5").style.color = "#053750";
	document.getElementById("sub6").style.color = "#053750";
	document.getElementById("sub7").style.color = "#053750";
	document.getElementById("sub8").style.color = "#053750";
	document.getElementById("sub9").style.color = "#053750";
	document.getElementById("container").style.color = "#FFFFFF";
	document.getElementById("s6").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("s7").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("s8").style.backgroundColor = " rgba(5,55,80,.5)";
	document.getElementById("header").style.backgroundColor = "#9CC61B";
	document.getElementById("send").style.backgroundColor = "#9CC61B";
	document.getElementById("yourname").addEventListener("focus", changeNameColor);
	document.getElementById("phone").addEventListener("focus", changePhoneColor);
	document.getElementById("emailField").addEventListener("focus", changeEmailColor);
	document.getElementById("website").addEventListener("focus", changeWebsiteColor);
	document.getElementById("message").addEventListener("focus", changeMessageColor);
	document.getElementById("yourname").addEventListener("blur", changeNameBlurColor);
	document.getElementById("phone").addEventListener("blur", changeBlurPhoneColor);
	document.getElementById("emailField").addEventListener("blur", changeBlurEmailColor);
	document.getElementById("website").addEventListener("blur", changeBlurWebsiteColor);
	document.getElementById("message").addEventListener("blur", changeBlurMessageColor);
	document.getElementById("linkedin").addEventListener("mouseover", changelinkedinColor);
	document.getElementById("emailLink").addEventListener("mouseover", changemailColor);
	document.getElementById("myBtn").addEventListener("mouseover", changeBtnColor);
	document.getElementById("linkedin").addEventListener("mouseout", changelinkedinMouseout);
	document.getElementById("emailLink").addEventListener("mouseout", changeEmailMouseout);
	document.getElementById("myBtn").addEventListener("mouseout", changeBtnMouseout);
}

function bigImg(x) {
	x.style.height = "180px";
	x.style.width = "180px";
}

function normalImg(x) {
	x.style.height = "160px";
	x.style.width = "160px";
}

function changeNameColor() {
	document.getElementById('yourname').style.borderBottomColor = header.style.backgroundColor;
	document.getElementById('yourname').style.color = header.style.backgroundColor;
}

function changeNameBlurColor() {
	document.getElementById('yourname').style.borderBottomColor = "white";

}

function changePhoneColor() {
	document.getElementById('phone').style.borderBottomColor = header.style.backgroundColor;
	document.getElementById('phone').style.color = header.style.backgroundColor;
}

function changeBlurPhoneColor() {
	document.getElementById('phone').style.borderBottomColor = "white";
}

function changeEmailColor() {
	document.getElementById('emailField').style.borderBottomColor = header.style.backgroundColor;
	document.getElementById('emailField').style.color = header.style.backgroundColor;
}

function changeBlurEmailColor() {
	document.getElementById('emailField').style.borderBottomColor = "white";
}

function changeWebsiteColor() {
	document.getElementById('website').style.borderBottomColor = header.style.backgroundColor;
	document.getElementById('website').style.color = header.style.backgroundColor;
}

function changeBlurWebsiteColor() {
	document.getElementById('website').style.borderBottomColor = "white";
}

function changeMessageColor() {
	document.getElementById('message').style.borderBottomColor = header.style.backgroundColor;
	document.getElementById('message').style.color = header.style.backgroundColor;
}

function changeBlurMessageColor() {
	document.getElementById('message').style.borderBottomColor = "white";
}

function changelinkedinColor() {
	document.getElementById('linkedin').style.color = header.style.backgroundColor;
}

function changemailColor() {
	document.getElementById('emailLink').style.color = header.style.backgroundColor;
}

function changeBtnColor() {
	document.getElementById('myBtn').style.color = header.style.backgroundColor;
}

function changelinkedinMouseout() {
	document.getElementById('linkedin').style.color = "rgb(222,222,222)";
}

function changeEmailMouseout() {
	document.getElementById('emailLink').style.color = "rgb(222,222,222)";
}

function changeBtnMouseout() {
	document.getElementById('myBtn').style.color = "#38393D";
}