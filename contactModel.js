
var mongoose = require('mongoose');
var contactSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    email: {type: String,
        required: true
    },
    websiteUrl:{type: String,
        required: true
    },
    message:{type: String,
        required: true
    },
    send_date: {
        type: Date,
        default: Date.now
    }
}, {
    versionKey: false 
});
var Contact = module.exports = mongoose.model('contact', contactSchema);
module.exports.get = function (callback, limit) {
    Contact.find(callback).limit(limit);
}